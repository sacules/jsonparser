# jsonparser

## Build
You'll need `yarn` and `node` installed, then you can build the project with:

```console
yarn install
yarn build
```

## Run
Once built, you can run it with:

```console
./bin/jsonparser --filepath "test.json" --key "blah"
```

## Test

```console
yarn test
```
