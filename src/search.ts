import { readFileSync } from "fs";
import { ANTLRInputStream, CommonTokenStream } from "antlr4ts";
import { ParseTreeWalker } from "antlr4ts/tree/ParseTreeWalker";

import CustomListener, { Results } from "./customlistener";
import { JSONLexer } from "./JSONLexer";
import { JSONParser } from "./JSONParser";
import { JSONListener } from "./JSONListener";

export function search(filepath: string, searchkey: string): Results {
  const listener = new CustomListener(searchkey);
  if (filepath === "" || searchkey === "") {
    return listener.searchResults;
  }

  let data: Buffer;

  try {
    data = readFileSync(filepath);
  } catch (e) {
    return listener.searchResults;
  }

  const input = data.toString();

  let chars = new ANTLRInputStream(input);
  let lexer = new JSONLexer(chars);
  let tokens = new CommonTokenStream(lexer);
  let parser = new JSONParser(tokens);

  // json is the tree's entrypoint
  let tree = parser.json();

  ParseTreeWalker.DEFAULT.walk(listener as JSONListener, tree);

  return listener.searchResults;
}
