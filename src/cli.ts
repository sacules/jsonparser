import yargs from "yargs";

export const args = yargs
  .option("filepath", { type: "string", demandOption: true, alias: "f" })
  .option("key", { type: "string", demandOption: true, alias: "k" }).argv;
