#!/usr/bin/env node

import { args } from "./cli";
import { search } from "./search";

function main() {
  const results = search(args.filepath, args.key);
  console.log(results);
}

main();
