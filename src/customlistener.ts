import { PairContext, ValueContext } from "./JSONParser";
import { JSONListener } from "./JSONListener";

export type Results = {
  start: number;
  end: number;
};

export default class CustomListener implements JSONListener {
  private searchkey: string;
  private found: boolean;
  public searchResults: Results;

  constructor(searchkey: string) {
    this.searchkey = searchkey;
    this.found = false;
    this.searchResults = { start: -1, end: -1 };
  }

  enterPair(ctx: PairContext) {
    let pair = ctx._start.text as string;

    // trim the quotes
    pair = pair.substring(1, pair.length - 1);
    if (pair === this.searchkey) {
      this.found = true; // next node in the AST is the value we're looking for
    }
  }

  enterValue(ctx: ValueContext) {
    if (!this.found) {
      return;
    }

    this.searchResults = {
      start: ctx._start.startIndex,
      end: ctx._start.stopIndex,
    };

    this.found = false; // no need to keep exploring the tree
  }
}
