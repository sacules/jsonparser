import { search } from "../src/search";

const testFile = "test.json";

describe("search for values in test.json", () => {
  it("get position of value for key 'hello'", () => {
    const results = search(testFile, "hello");
    expect(results.start).toEqual(11);
    expect(results.end).toEqual(17);
  });

  it("get position of value for key 'blah'", () => {
    const results = search(testFile, "blah");
    expect(results.start).toEqual(28);
    expect(results.end).toEqual(29);
  });

  it("get position of value for key 'foo'", () => {
    const results = search(testFile, "foo");
    expect(results.start).toEqual(-1);
    expect(results.end).toEqual(-1);
  });

  it("get position of value for empty key", () => {
    const results = search(testFile, "");
    expect(results.start).toEqual(-1);
    expect(results.end).toEqual(-1);
  });
});

describe("search for value in missing file", () => {
  it("get position of value for key 'foo' on empty filepath", () => {
    const results = search("", "foo");
    expect(results.start).toEqual(-1);
    expect(results.end).toEqual(-1);
  });

  it("get position of value for key 'foo' on non-existant file", () => {
    const results = search("bar.json", "foo");
    expect(results.start).toEqual(-1);
    expect(results.end).toEqual(-1);
  });
});
